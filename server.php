<?php
	require_once('lib/nusoap.php');    // Se incluye la librería nusoap

	//Creación i Configuración del objeto soap_server
	$server = new soap_server;
	$server->configureWSDL('phpcentral', 'urn:phpcentral');
	$server->wsdl->schemaTargetNamespace = 'urn:phpcentral';

	//Función o Proceso del Webservice (Declaración)
	function saludar($nombre) {
		if (empty($nombre)) {
			return new soap_fault('Cliente', '', 'Ingrese su nombre!');
		}
		return "Hola " . $nombre;
	}

	//Registro de la función del Webservice
	$server->register(
			'saludar' 							// <-- Nombre de la función
			, array('nombre' => 'xsd:string') 	// <-- Parámetro de entrada (un string)
			, array('return' => 'xsd:string')   // <-- parámetro de retorno (un string)
	);

	// Se debe crear el HTTP listener
	$HTTP_RAW_POST_DATA = (isset($HTTP_RAW_POST_DATA)) ? $HTTP_RAW_POST_DATA : '';
	$server->service($HTTP_RAW_POST_DATA);
	exit();
?>