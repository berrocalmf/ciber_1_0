<?php
    require_once('lib/nusoap.php');    // Librería nusoap (Web service tipo SOAP)
    include_once("database.class.php");
    
    // Creación y Configuración del objeto soap_server
	$server = new soap_server;
	$server->configureWSDL('hclinica', 'urn:hclinica');
	$server->wsdl->schemaTargetNamespace = 'urn:hclinica';

    // Función o Proceso del Webservice (Declaración)
	function transmitir($afiid,$idmedico,$fechahc,$fini,$ffin,$sql1,$sql2,$sql3,$usuario) {

        // Validaciones que realiza el Servicio Web a los datos para iniciar el proceso ...

		if (empty($afiid)) {
			return new soap_fault('Cliente', '', 'Par&aacute;metro Id. Afiliado no v&aacute;lido!');
        }
        if (empty($idmedico)) {
			return new soap_fault('Cliente', '', 'Par&aacute;metro Id. M&eacute;dico no v&aacute;lido!');
        }
        if (empty($fechahc)) {
			return new soap_fault('Cliente', '', 'Par&aacute;metro Fecha Historia no v&aacute;lido!');
        }
        if (empty($fini)) {
			return new soap_fault('Cliente', '', 'Par&aacute;metro Fecha Inicio no v&aacute;lido!');
        }
        if (empty($ffin)) {
			return new soap_fault('Cliente', '', 'Par&aacute;metro Fecha Fin no v&aacute;lido!');
        }
        if (empty($sql1)) {
			return new soap_fault('Cliente', '', 'Historia Cl&iacute;nica sin Datos!');
        }
        if (empty($sql2)) {
			return new soap_fault('Cliente', '', 'Historia Cl&iacute;nica sin Campos!');
        }
        if (empty($usuario)) {
			return new soap_fault('Cliente', '', 'Par&aacute;metro Usuario no v&aacute;lido!');
        }
           
        // El Web Service se conecta con la Base de Datos
        $conn = new Database();                     

        // Acá se realiza el control de la existencia de una cita en la base de datos destino para realizar 
        // la sincronización o la trasncripción.

        // comentar para pruebas ...  

        $consulta_cit  = "SELECT cit.CONSECUTIVO,cit.CITID,cit.FECHA FROM cit WHERE cit.IDAFILIADO='{$afiid}' AND ";
        $consulta_cit .= " cit.IDMEDICO='{$idmedico}' AND CIT.FECHA BETWEEN '{$fini}' AND '{$ffin}' AND ";
        $consulta_cit .= " cit.CUMPLIDA=0";

        $sth = $conn->prepare($consulta_cit);
        $sth->execute();
        $res = $sth->fetchall(PDO::FETCH_ASSOC);

        if(!$res) {
            return new soap_fault('Cliente', '', 'No hay cita para Paciente id ' . $afiid);
        }

        // El sistema toma los datos de la cita
        foreach ($res as $row) {
            $cnscita  = $row['CONSECUTIVO'];
            $citid    = $row['CITID'];
            $fechacit = $row['FECHA'];
        }

        if (empty($cnscita)) {
            return new soap_fault('Cliente', '', 'Par&aacute;metro consecutivo cita no v&aacute;lido!');
        }
        if (empty($citid)) {
			return new soap_fault('Cliente', '', 'Par&aacute;metro Id. cita no v&aacute;lido!');
        }
        if (empty($fechacit)) {
			return new soap_fault('Cliente', '', 'Par&aacute;metro Fecha cita no v&aacute;lido!');
        }

        // Comenta hasta acá ...

        // asignamos valores a variables de cita para pruebas
        // $cnscita  = '3400079976';
        // $citid    = 13;
        // $fechacit = '2018-29-01 07:40:00.000';

        $sql = "exec SP_SINCRO_HCA ?, ?, ?, ?, ?, ?, ?";
        $query=$conn->prepare($sql);
        $parametros = array($cnscita,$fechacit,$afiid,$sql1,$sql2,$sql3,$usuario);
        
        try {
            $respuesta = $query->execute($parametros);
        } catch (Exception $ex) {
            return new soap_fault('Cliente', '', 'Error Llamado a Stored Procedure' . $ex );
        }

        if (!$respuesta) {
            return new soap_fault('Cliente', '', 'Registro de H. Cl&iacute;nica no actualizado');
        }

        // ... Habilitar la creación del Afiaido en CYBER 1.0 en windev
		return "OK";
    }
    
    // Parámetros $afiid,$idmedico,$fechahc,$fini,$ffin,$sql1,$sql2,$sql3,$usuario
	// Registro de la función del Webservice
	$server->register(
			'transmitir' 							// <-- Nombre de la función
            , array('afiid' => 'xsd:string','idmedico' => 'xsd:string','fechahc' => 'xsd:string','fini' => 'xsd:string','ffin' => 'xsd:string','sql1' => 'xsd:string','sql2' => 'xsd:string','sql3' => 'xsd:string','usuario' => 'xsd:string')
			, array('return' => 'xsd:string')       // <-- parámetro de retorno (un string)
	);

    // [ Código Obsoleto para PHP v 7 ]    
        // Se debe crear el HTTP listener
        // $HTTP_RAW_POST_DATA = (isset($HTTP_RAW_POST_DATA)) ? $HTTP_RAW_POST_DATA : '';
        // $server->service($HTTP_RAW_POST_DATA);  // Esta forma entró en desuso en PHP 5.0

    $server->service(file_get_contents("php://input"));     // Nueva forma
	exit();
?>